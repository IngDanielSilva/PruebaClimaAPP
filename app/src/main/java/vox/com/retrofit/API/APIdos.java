package vox.com.retrofit.API;

import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vox.com.retrofit.API.Deserializers.MyDeserializer;
import vox.com.retrofit.API.Deserializers.MyDeserializeruno;
import vox.com.retrofit.Models.City;

/**
 * Created by DanielMartin on 30/01/2018.
 */

public class APIdos {

    private static Retrofit retrofit = null;
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    public static Retrofit getApi(){
        if(retrofit == null){
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(City.class,new MyDeserializeruno());

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .build();
        }
        return retrofit;
    }
}
