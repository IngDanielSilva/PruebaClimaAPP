package vox.com.retrofit.API.Deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import vox.com.retrofit.Models.City;

/**
 * Created by DanielMartin on 30/01/2018.
 */

public class MyDeserializeruno implements JsonDeserializer<City> {

@Override
public City deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        String[] temp = new String[5];
        String[] description = new String[5];
        String[] icon = new String[5];
        String[] tempMax = new String[5];
        String[] tempMin = new String[5];


        int id = 1;
        String name = json.getAsJsonObject().get("name").getAsString();
        String county = json.getAsJsonObject().get("sys").getAsJsonObject().get("country").getAsString();
        icon[0] = json.getAsJsonObject().getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("icon").getAsString();
        temp[0] = json.getAsJsonObject().getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsString();
        tempMax[0] = json.getAsJsonObject().getAsJsonObject().get("main").getAsJsonObject().get("temp_max").getAsString();
        tempMin[0] = json.getAsJsonObject().getAsJsonObject().get("main").getAsJsonObject().get("temp_min").getAsString();
        description[0] = json.getAsJsonObject().getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("description").getAsString();;
        City city = new City(id, name, county, icon, temp,tempMax,tempMin, description);

        return city;
        }

}
